//
//  AppDelegate.swift
//  Piggy Store
//
//  Created by Pasha Cretsu on 1/23/18.
//  Copyright © 2018 Noname. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

}
