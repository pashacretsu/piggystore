//
//  MSButton.swift
//  Piggy Store
//
//  Created by Pasha Cretsu on 1/23/18.
//  Copyright © 2018 SharpMinds. All rights reserved.
//

import UIKit

@IBDesignable final class MSButton: UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    
    open var isAnimated: Bool = false
    
    
    private lazy var activityIndicator:UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .white)
        activityIndicator.alpha = 0
        activityIndicator.color = .white
        self.addSubview(activityIndicator)
        
        return activityIndicator
    }()
    
    
    override func draw(_ rect: CGRect) {
        if let titlelabel = self.titleLabel, titlelabel.text != nil {
            activityIndicator.center = CGPoint(x: titlelabel.frame.origin.x - activityIndicator.frame.size.width, y: titlelabel.center.y)
        }
        
        if let imageView = self.imageView, imageView.image != nil {
            activityIndicator.center = imageView.center
        }
    }
    
    
    func startAnimation(withText text:String? = .none) {
        if let newTitle = text {
            setTitle(newTitle, for: .normal)
        }
        isActivityIndicator(animated: true)
    }
    
    
    func stopAnimation(withText text:String? = .none) {
        if let newTitle = text {
            setTitle(newTitle, for: .normal)
        }
        isActivityIndicator(animated: false)
    }
    
    
    private func isActivityIndicator(animated: Bool) {
        isAnimated = animated
        isEnabled = !animated
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = animated
        
        UIView.animate(withDuration: 0.3) {
            self.activityIndicator.alpha = (animated == true) ? 1 : 0
        }
        
        if animated {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
}
