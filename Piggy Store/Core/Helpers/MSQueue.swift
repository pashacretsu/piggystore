//
//  MSQueue.swift
//  Piggy Store
//
//  Created by Pasha Cretsu on 1/23/18.
//  Copyright © 2018 SharpMinds. All rights reserved.
//

import Foundation

enum Queue: String {
    
    private static var pools = [String: DispatchQueue]()
    
    case database  = "com.piggy.databaseQueue"
    case transform = "com.piggy.transformQueue"
    case network   = "com.piggy.networkQueue"
    case ui        = "com.piggy.uiQueue"
    
    
    /// Create dispatch queue.
    ///
    /// - Returns: DispatchQueue instance
    func create() -> DispatchQueue {
        if self == .ui {
            return DispatchQueue.main
        } else {
            let qos: DispatchQoS =
                self == .database ? .utility
                    : self == .transform ? .userInitiated
                    : self == .network ? .userInitiated
                    : .background
            return DispatchQueue(label: self.rawValue, qos: qos)
        }
    }
    
    
    /// Submits a block for asynchronous execution on a dispatch queue.
    ///
    /// - Parameter execute: work item for execution
    func async(block: @escaping () -> ()) {
        let job: DispatchWorkItem = DispatchWorkItem(block: block)
        let queue = Queue.pools[rawValue] ?? {
            Queue.pools[rawValue] = $0
            return $0
            }(create())
        
        queue.async(execute: job)
    }
}
