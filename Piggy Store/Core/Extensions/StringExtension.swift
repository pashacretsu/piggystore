//
//  StringExtension.swift
//  Piggy Store
//
//  Created by Pasha Cretsu on 1/23/18.
//  Copyright © 2018 SharpMinds. All rights reserved.
//

import Foundation

extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    
    var length: Int {
        return self.count
    }
}
