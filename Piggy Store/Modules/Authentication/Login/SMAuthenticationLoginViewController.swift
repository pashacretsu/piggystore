//
//  SMAuthenticationLoginViewController.swift
//  Piggy Store
//
//  Created by Pasha Cretsu on 1/23/18.
//  Copyright © 2018 SharpMinds. All rights reserved.
//

import UIKit

protocol SMAuthenticationLoginUIProtocol {
    
    func cleanupForm()
    
    func hideKeyboard()
    
    func successfullyLoggedIn()
    
    func setUserInteraction(enabled: Bool)
    
    func displayError(title: String, message: String)
}

class SMAuthenticationLoginViewController: UIViewController {
    
    fileprivate let maxInputLength = 32
    
    internal var presenter: SMAuthenticationLoginPresenterIn?
    
    @IBOutlet weak var logoImg: UIImageView!
    
    @IBOutlet weak var logoLbl: UILabel!
    
    @IBOutlet weak var usernameFld: UITextField!
    
    @IBOutlet weak var passwordFld: UITextField!
    
    @IBOutlet weak var togglePasswordBtn: UIButton!
    
    @IBOutlet weak var loginBtn: MSButton!
    
    @IBOutlet weak var centerYConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var topAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let presenter = SMAuthenticationLoginPresenter()
        self.presenter = presenter
        presenter.view = self
        let interactor = SMAuthenticationLoginInteractor()
        presenter.interactor = interactor
        interactor.presenter = presenter
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        localizeUI()
        cleanupForm()
        isKeyboard(shown: false)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let notification = NotificationCenter.default
        notification.addObserver(self, selector: #selector(moveKeyboardInResponceToShow(notofocation:)), name: .UIKeyboardWillShow, object: nil)
        notification.addObserver(self, selector: #selector(moveKeyboardInResponceToHide(notofocation:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    
    @objc func moveKeyboardInResponceToShow(notofocation: Notification) {
        isKeyboard(shown: true)
    }
    
    
    @objc func moveKeyboardInResponceToHide(notofocation: Notification) {
        isKeyboard(shown: false)
    }
    
    
    private func isKeyboard(shown: Bool) {
        //Keep constraint order
        if shown {
            centerYConstraint.isActive = false
            topAnchor.isActive = true
        } else {
            topAnchor.isActive = false
            centerYConstraint.isActive = true
        }
        
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.logoLbl.font = self?.logoLbl.font.withSize(shown ? 42.0: 74.0)
            self?.contentViewHeight.constant = shown ? 320.0 : 382.0
            self?.view.layoutIfNeeded()
        }
    }
    
    
    private func localizeUI() {
        logoLbl.text = "General → piggy".localized
        usernameFld.placeholder = "Authorization → Username".localized
        passwordFld.placeholder = "Authorization → Password".localized
        togglePasswordBtn.setTitle((passwordFld.isSecureTextEntry ? "Authorization → Show" : "Authorization → Hide").localized, for: .normal)
        loginBtn.setTitle("Authorization → Login".localized, for: .normal)
    }

    
    @IBAction func loginBtnTapped(_ sender: MSButton) {
        presenter?.loginWith(username: usernameFld.text, password: passwordFld.text)
    }
    
    
    @IBAction func onViewTapped(_ sender: UITapGestureRecognizer) {
        presenter?.viewTapped()
    }
}

extension SMAuthenticationLoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            passwordFld.becomeFirstResponder()
        } else {
            view.endEditing(true)
            presenter?.loginWith(username: usernameFld.text, password: passwordFld.text)
        }
        
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength:Int = (textField.text?.length)! + string.length - range.length
        
        return !(newLength > maxInputLength)
    }
}

extension SMAuthenticationLoginViewController: SMAuthenticationLoginUIProtocol {
    
    func cleanupForm() {
        usernameFld.text = .none
        passwordFld.text = .none
    }
    
    
    func hideKeyboard() {
        view.endEditing(true)
        
    }
    
    
    func successfullyLoggedIn() {
        showAlert(title:"Success", message: "Now you are in")
    }
    
    
    func setUserInteraction(enabled: Bool) {
        view.isUserInteractionEnabled = enabled
        usernameFld.isEnabled = enabled
        passwordFld.isEnabled = enabled
        if enabled == true {
            loginBtn.stopAnimation()
        } else {
            loginBtn.startAnimation()
        }
    }
    
    
    func displayError(title: String, message: String) {
        showAlert(title:title, message: message)
    }
}
