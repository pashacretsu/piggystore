//
//  SMAuthenticationLoginInteractor.swift
//  Piggy Store
//
//  Created by Pasha Cretsu on 1/23/18.
//  Copyright © 2018 SharpMinds. All rights reserved.
//

import Foundation

protocol SMAuthenticationLoginInteractorProtocol {
    
    func loginWith(username: String?, password: String?)
}

class SMAuthenticationLoginInteractor {

    var presenter: SMAuthenticationLoginPresenterOut?
}

extension SMAuthenticationLoginInteractor: SMAuthenticationLoginInteractorProtocol {

    func loginWith(username: String?, password: String?) {
        guard let username = username, username.isEmpty == false else {
            presenter?.displayError(title: "Authorization → Validation error".localized, message: "Authorization → Username value cannot be empty".localized)
            return
        }
        
        guard let password = password, password.isEmpty == false else {
            presenter?.displayError(title: "Authorization → Validation error".localized, message: "Authorization → Password value cannot be empty".localized)
            return
        }
        
        //Disable user interaction for execution time
        presenter?.setUserInteraction(enabled: false)
        
        //Reset UI after whatever result
        let enableUserInterface: () -> () = { [weak self] in
            self?.presenter?.setUserInteraction(enabled: true)
        }
        
        //Simulate request dellay
        let deadlineTime = DispatchTime.now() + 3.0
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) { [weak self] in
            //Real server-server credentials check simulation
            if username == "piggy demo" && password == "wachtwoord" {
                enableUserInterface()
                self?.presenter?.successfullyLoggedIn()
            } else {
                enableUserInterface()
                self?.presenter?.displayError(title: "Authorization → Authorization error".localized, message: "Authorization → Invalid username or password".localized)
            }
        }
    }
}
