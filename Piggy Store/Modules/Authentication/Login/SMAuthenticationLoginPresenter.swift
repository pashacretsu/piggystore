//
//  SMAuthenticationLoginPresenter.swift
//  Piggy Store
//
//  Created by Pasha Cretsu on 1/23/18.
//  Copyright © 2018 SharpMinds. All rights reserved.
//

import Foundation

protocol SMAuthenticationLoginPresenterIn {
    
    func viewTapped()
    
    func loginWith(username: String?, password: String?)
}


protocol SMAuthenticationLoginPresenterOut {
    
    func successfullyLoggedIn()
    
    func setUserInteraction(enabled: Bool)
    
    func displayError(title: String, message: String)
}


class SMAuthenticationLoginPresenter {
    
    var interactor: SMAuthenticationLoginInteractorProtocol?
    
    var view: SMAuthenticationLoginUIProtocol?
}

extension SMAuthenticationLoginPresenter: SMAuthenticationLoginPresenterIn {
    
    func viewTapped() {
        Queue.ui.async { [weak self] in
            self?.view?.hideKeyboard()
        }
    }
    
    func loginWith(username: String?, password: String?) {
        Queue.network.async {[weak self] in
            self?.interactor?.loginWith(username: username, password: password)
        }
    }
}

extension SMAuthenticationLoginPresenter: SMAuthenticationLoginPresenterOut {
    
    func successfullyLoggedIn() {
        Queue.ui.async { [weak self] in
            self?.view?.cleanupForm()
            self?.view?.successfullyLoggedIn()
        }
    }
    
    
    func displayError(title: String, message: String) {
        Queue.ui.async { [weak self] in
            self?.view?.cleanupForm()
            self?.view?.displayError(title: title, message: message)
        }
    }
    
    
    func setUserInteraction(enabled: Bool) {
        Queue.ui.async { [weak self] in
            self?.view?.setUserInteraction(enabled: enabled)
        }
    }
}
